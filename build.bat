@echo off
setlocal EnableDelayedExpansion

echo                                        ______
echo        /\                             ^|  ____^|
echo       /  \    _ __  _ __ ___    __ _  ^| ^|__  ___   _ __  ___  ___  ___
echo      / /\ \  ^| '__^|^| '_ ` _ \  / _` ^| ^|  __^|/ _ \ ^| '__^|/ __^|/ _ \/ __^|
echo     / ____ \ ^| ^|   ^| ^| ^| ^| ^| ^|^| (_^| ^| ^| ^|  ^| (_) ^|^| ^|  ^| (__^|  __/\__ \
echo    /_/    \_\^|_^|   ^|_^| ^|_^| ^|_^| \__,_^| ^|_^|   \___/ ^|_^|   \___^|\___^|^|___/
echo.


set private_key=ArmaForces_No_Actions.biprivatekey
set public_key=ArmaForces_No_Actions.bikey
set mod_base=@armaforces_no_actions

CALL :build_folder addons || exit /B 1

exit /B 0

:build_folder
    set mod=%mod_base%_%~1

    if not exist "%mod%\keys" mkdir "%mod%\keys"
    copy /Y %public_key% "%mod%\keys\"

    if not exist "%mod%\addons" mkdir "%mod%\addons"
    del /f /s /q "%mod%\addons" 1>nul

    echo ==== Build ====
    for /D %%i in (".\%~1\*") do (
        set pbo=armaforces_%%~ni.pbo
        echo [Building] !pbo! from %%i
        call .\tools\armake2.exe build -x *.tga -i P:\ "%%i" ".\%mod%\addons\!pbo!"

        if !errorlevel! neq 0 (
            echo [!pbo!] Failed
            exit /B !errorlevel!
        ) else (
            echo [!pbo!] Ok
        )
        echo.
    )

    if exist %private_key% (
        echo ==== Sign ====
        for %%p in (.\%mod%\addons\*.pbo) do (
            echo [Signing] %%p
            call .\tools\armake2.exe sign .\%private_key% "%%p"

            if !errorlevel! neq 0 (
                echo Failed
                exit /B !errorlevel!
            ) else (
                echo Ok
            )
            echo.
        )
    ) else (
        echo Key not found: %private_key%
        echo Skipping signing.
    )


    echo [Finished]
    echo.
    exit /B 0

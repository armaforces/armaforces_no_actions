# ArmaForces - No Actions

[Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=1682845363)  
[Download](https://gitlab.com/armaforces/armaforces_no_actions/-/jobs/artifacts/master/download?job=build)

## Content

This addon removes useless action menu actions.

- Eject (enabled by default)
- Rearm (must be enabled in CBA Settings)

## Development environment

It is recommended to use Visual Studio Code with following extensions:
 * [SQFLint](https://marketplace.visualstudio.com/items?itemName=skacekachna.sqflint) - SQF Linting and completion

For addon building it is reccommended to use [armake2](https://github.com/KoffeinFlummi/armake2).

The repository contains `build.bat` file that will use armake2 from "tools" dir to build the addon.  
If `ArmaForces_No_Actions.biprivatekey` will be present in root of the repo, then built PBOs will be signed with this key.

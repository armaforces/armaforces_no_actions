#define QUOTE(VAR)          #VAR
#define ARR_2(ARG1, ARG2)   [ARG1, ARG2]

#define AF_NO_ACTION_CONIG_ENTRY(NAME, DEFAULT)\
class NAME: None \
{\
    show = QUOTE(call compile getText (configFile >> 'CfgActions' >> 'NAME' >> 'armaforces_setting'));\
    armaforces_setting = QUOTE(profileNamespace getVariable ARR_2('armaforces_no_actions_show_##NAME', DEFAULT));\
}\

class CfgActions
{
    class None;

    AF_NO_ACTION_CONIG_ENTRY(Eject, 0);
    AF_NO_ACTION_CONIG_ENTRY(Rearm, 1);
    AF_NO_ACTION_CONIG_ENTRY(TurnIn, 1);
    AF_NO_ACTION_CONIG_ENTRY(TurnOut, 1);
    AF_NO_ACTION_CONIG_ENTRY(LightOn, 1);
    AF_NO_ACTION_CONIG_ENTRY(LightOff, 1);
};

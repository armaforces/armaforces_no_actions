class CfgPatches
{
    class armaforces_no_actions
    {
        name = "ArmaForces - No Actions";
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"A3_Data_F_Tank_Loadorder", "cba_main"};
        author = "veteran29";
    };
};

#include "CfgEventHandlers.hpp"
#include "CfgActions.hpp"
